from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite3'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False 
db = SQLAlchemy(app)

class Joueur(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    prenom = db.Column(db.String(50))
    nom = db.Column(db.String(50))

db.create_all()
    
@app.route('/')
def index():
    return render_template('index.html', contenu="hello")

@app.route('/admin')
def admin():
    return render_template('index.html', contenu="Admin")

@app.route('/admin/update', methods=['POST'])
def update_joueur():
    if request.form['action'] == 'add':
        joueur = Joueur(prenom=request.form['prenom'], nom=request.form['nom'])
        db.session.add(joueur)
        db.session.commit()
    elif request.form['action'] == 'del':
        user = Joueur.query.get(request.form['user_id'])
        if user:
            db.session.delete(user)
            db.session.commit()
    liste_joueurs = Joueur.query.all()
    return render_template('update.html', joueurs=liste_joueurs)


@app.route('/admin/joueurs')
def liste():
    liste_joueurs = Joueur.query.all()
    return render_template('liste.html', joueurs=liste_joueurs)
    